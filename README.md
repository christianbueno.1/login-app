# Learning to use flask-login, flask-sqlalchemy, sqlalchemy

## Step to run the web

```python
#bash shell
#clone the repository
git clone https://gitlab.com/christianbueno.1/login-app
cd login-app
#Python verion 3.9.7 , by default installed in fedora-34-KDE
python -m venv env
source env/bin//activate
pip install -r requirements.txt
export FLASK_APP=loginpkg
export FLASK_ENV=development
flask init-db
flask run
#by defalut in localhost and port 5000
#open any browser in http://127.0.0.1:5000/signup
```
## Create a user
1. got the url http://127.0.0.1:5000/signup
2. type a email, name and password

## Resources
* Tutorial from [digital ocean website](https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login) 

## Have to review it in the future
* Form validation with WTForms
