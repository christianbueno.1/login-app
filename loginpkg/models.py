import click
from . import db
from flask_sqlalchemy import SQLAlchemy
from flask.cli import with_appcontext
from datetime import date, datetime
#is necessary in the model, session cookie
from flask_login import UserMixin

# db = SQLAlchemy()

class User(UserMixin, db.Model):
    __tablename__= "users"

    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    #email is unique, no repetition
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    dob = db.Column(db.Date)
    created_on = db.Column(db.DateTime, default=datetime.now)
    updated_on = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)
    price = db.Column(db.Numeric(13,4))

    def __repr__(self):
        return f'<User {self.name}>'
    
    # def __init__(self,ids, email, password):
    def __init__(self, email, name, password):
        # self.id = ids
        self.email = email
        self.name = name
        self.password = password
        
# from terminal ru: flask init-db
#database cli command, init-db is the command
@click.command('init-db')
@with_appcontext
def init_db_command():
    db.create_all()
    click.echo('Initialized the database')

def init_app(app):
    app.cli.add_command(init_db_command)