from flask import Blueprint
from flask.templating import render_template
from flask import Blueprint, redirect, url_for, request, flash

# from flask_login.utils import login_required, logout_user
from flask_login import login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from.models import User
from . import db
# from .models import db
#session cookie

auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    remember: bool
    email = request.form.get('email')
    # email = request.form['email']
    print(f"request.form.get('remember'): {request.form.get('remember')}")
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False
    #query sqlalchemy, database data
    user = User.query.filter_by(email=email).first()
    #check_password_hash
    if not (user and check_password_hash(user.password, password) ):
        flash('Please check your login details and try again.')
        #redirect is a request, GET method
        return redirect(url_for('auth.login'))

    #login sucess, save the user, you must close the web browser
    # to lose the session
    # remember=True, prevent being logged out when close the browser
    login_user(user, remember=remember)
    return redirect(url_for('main.profile'))

@auth.route('/signup')
def signup():
    return render_template('signup.html')

@auth.route('/logout')
@login_required
def logout():
    logout_user()

    #if use render_template measn that you will type the url
    #then use here redirect
    # return render_template('login.html') 
    #and url_for('blueprint.function')

    # return redirect(url_for('auth.login')) 
    return redirect(url_for('main.index')) 

@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first()
    if user:
        #message to the next request
        flash('Email address already exists')
        return redirect(url_for('auth.signup'))

    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))